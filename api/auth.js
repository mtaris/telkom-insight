import axios from "axios";

export const signInWithGoogle = (code) => {
  return axios.post(`https://oauth2.googleapis.com/token`, {
    client_id: process.env.NEXT_PUBLIC_CLIENT_ID_GOOGLE,
    client_secret: process.env.NEXT_PUBLIC_CLIENT_SECRET_GOOGLE,
    code,
    grant_type: "authorization_code",
    redirect_uri: process.env.NEXT_PUBLIC_HOST_URL + "/login",
  });
};

export const getGoogleProfile = (accessToken) => {
  return axios.get(
    `https://www.googleapis.com/oauth2/v1/userinfo?access_token=${accessToken}` // Setelah berhasil auth, kemudian dapet token, mau kehalaman mana?
  );
};

// export const signInWithGitlab = (code) => {
//   return axios.post(`https://git.digitalamoeba.id/oauth/token`, {
//     client_id: process.env.NEXT_PUBLIC_CLIENT_ID_GITLAB,
//     client_secret: process.env.NEXT_PUBLIC_CLIENT_SECRET_GITLAB,
//     code,
//     grant_type: "authorization_code",
//     redirect_uri: process.env.NEXT_PUBLIC_HOST_URL + "/login",
//   });
// };

// export const getGitlabProfile = (accessToken) => {
//   return axios.get(
//     `https://git.digitalamoeba.id/api/v4/user?access_token=${accessToken}` // Setelah berhasil auth, kemudian dapet token, mau kehalaman mana?
//   );
// };
