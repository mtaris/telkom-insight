const STAGING_API = "https://staging-telkom-insights.herokuapp.com";
const PRODUCTION_API = "https://telkom-insights.herokuapp.com";

export { STAGING_API, PRODUCTION_API };
