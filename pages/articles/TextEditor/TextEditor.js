import React, { useState, useEffect } from "react";
import dynamic from "next/dynamic";
import PropTypes from "prop-types";
import { useRouter } from "next/router";
//import the component
const RichTextEditor = dynamic(() => import("react-rte"), { ssr: false });

const MyStatefulEditor = ({ onChange, value, setValue, text }) => {
  console.log("VALUE", value !== undefined && value.toString("html"));
  console.log("TEXT", text);
  const router = useRouter();
  useEffect(() => {
    const importModule = async () => {
      //import module on the client-side to get `createEmptyValue` instead of a component
      const modules = await import("react-rte");
      !text
        ? setValue(modules.createEmptyValue())
        : setValue(modules.createValueFromString(text, "html"));
    };
    importModule();
  }, [router.pathname]);

  const handleOnChange = (value) => {
    setValue(value);
    if (onChange) {
      onChange(value.toString("html"));
    }
  };

  if (!value) {
    return null;
  }

  return <RichTextEditor value={value} onChange={handleOnChange} />;
};

// MyStatefulEditor.propTypes = {
//   onChange: PropTypes.func,
// };

export default MyStatefulEditor;
