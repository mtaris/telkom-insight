import axios from "axios";
import { useEffect, useState, useContext } from "react";
import Router from "next/router";
import moment from "moment";
import { Modal, Button } from "react-bootstrap";
import { useRouter } from "next/router";
import Link from "next/link";
import img from "next/image";
import Head from "next/head";
import TextEditor from "../TextEditor/TextEditor";
import parse from "html-react-parser";
import { toast } from "react-toastify";
import { AuthContext } from "../../../components/authContext";
import { STAGING_API } from "../../../api/axiosUrl";

function EditArticle() {
  const authContext = useContext(AuthContext);
  const router = useRouter();
  const { id } = router.query;
  const [title, setTitle] = useState("");
  const [content, setContent] = useState();
  const [program, setProgram] = useState({});
  const [image, setImage] = useState(null);
  const [imagePreview, setImagePreview] = useState(null);
  const [fullscreen, setFullscreen] = useState(true);
  const [show, setShow] = useState(false);
  const [previewContent, setPreviewContent] = useState("");
  const [allPrograms, setAllPrograms] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [oneArticle, setOneArticle] = useState({});
  const [oneContent, setOneContent] = useState("");

  console.log("isi program", program.name);

  const getDetailArticle = async () => {
    try {
      const response = await axios.get(`${STAGING_API}/api/v1/articles/${id}`);
      setOneArticle(response.data.data);
      setTitle(response.data.data.title);
      setProgram(response.data.data.program);
      setImagePreview(response.data.data.image.link);
      setOneContent(response.data.data.content.toString("html"));
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const getAllPrograms = async () => {
    try {
      const response = await axios.get(
        `${STAGING_API}/api/v1/programs`,

        { headers: { Authorization: `Bearer ${localStorage.accessToken}` } }
      );
      setAllPrograms(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };
  const selectOneProgram = async (e) => {
    try {
      const response = await axios.get(
        `${STAGING_API}/api/v1/programs/${e}`,

        { headers: { Authorization: `Bearer ${localStorage.accessToken}` } }
      );
      setProgram(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  const onChangeImageFile = (e) => {
    setImage(e.target.files[0]);
    const reader = new FileReader();
    reader.onload = () => {
      if (reader.readyState === 2) {
        setImagePreview(reader.result);
      }
    };
    reader.readAsDataURL(e.target.files[0]);
  };

  const submitEditArticle = async () => {
    setLoadingSubmit(true);
    try {
      const formData = new FormData();
      formData.append("title", title);
      formData.append("content", content.toString("html"));
      formData.append("tags", program.name);
      formData.append("program", program._id);
      if (image) {
        formData.append("image", image);
      }
      const response = await axios.put(
        `${STAGING_API}/api/v1/articles/${id}`,
        formData,
        {
          headers: {
            Authorization: `Bearer ${localStorage.accessToken}`,
            "content-type": "multipart/form-data",
          },
        }
      );
      console.log(response);
      Router.replace("/articles/" + oneArticle._id);
      setLoadingSubmit(false);
      toast.success("Artikel berhasil di update!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
    } catch (error) {
      setLoadingSubmit(false);
      toast.error(error.response.data.messages, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
      console.error(error.response.data.messages);
    }
  };

  useEffect(() => {
    getAllPrograms();
    getDetailArticle();
    if (authContext.contextLoading === false) {
      authContext.isAuth ? null : Router.push("/loginadmin");
    }
  }, []);

  if (loading) {
    return (
      <div className="container py-5 h-100 text-center col-lg-6 col-md-12">
        <div className="col mb-4">
          <div
            className="card shadow border-0"
            style={{ height: 70, backgroundColor: "lightgray" }}
          ></div>
        </div>
        <div className="col">
          <div
            className="card shadow border-0"
            style={{ height: "100vh", backgroundColor: "lightgray" }}
          ></div>
        </div>
      </div>
    );
  }

  const handleShow = (breakpoint) => {
    setFullscreen(breakpoint);
    setShow(true);
    if (content) {
      return setPreviewContent(content.toString("html"));
    }
  };
  return (
    <div>
      <Head>
        <title>Edit Article | Telkom Insights</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="stylesheet"
          type="text/css"
          href="//fonts.googleapis.com/css?family=Open+Sans"
        />
      </Head>
      <section>
        <div className="container py-5 h-100 col-lg-6 col-md-12">
          <h3>Edit Artikel</h3>
          <hr />
          <div className="mb-3">
            <label htmlFor="exampleFormControlInput1" className="form-label">
              Judul artikel
            </label>
            <input
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              type="text"
              className="form-control"
              id="exampleFormControlInput1"
              placeholder="Judul artikel"
            />
          </div>
          <div className="mb-3 col-4">
            <small>Gambar saat ini</small>
            <img
              src={oneArticle.image !== null && imagePreview}
              className="img-fluid"
              alt="..."
            />
            <label htmlFor="formFile" className="form-label">
              Update gambar
            </label>
            <input
              className="form-control"
              type="file"
              id="formFile"
              onChange={onChangeImageFile}
            />
          </div>
          <br />
          <div className="mb-3">
            <p>Content artikel</p>
            <TextEditor
              value={content}
              setValue={setContent}
              text={oneArticle.content}
            />
          </div>
          <div className="mb-3 col-3">
            <label htmlFor="exampleFormControlInput1" className="form-label">
              Pilih program
            </label>
            <select
              className="form-select"
              aria-label="Default select example"
              onChange={(f) => selectOneProgram(f.target.value)}
            >
              <option selected disabled>
                Pilih salah satu program
              </option>
              {allPrograms.map((e, index) => (
                <option
                  value={e._id}
                  key={index}
                  style={{ color: "black" }}
                  selected={program.name === e.name ? true : false}
                >
                  {e.name}
                </option>
              ))}
            </select>
            <span className="badge bg-secondary mt-2">{program.name}</span>
          </div>
          <div className="d-flex justify-content-end mt-3">
            <button
              onClick={() => Router.back()}
              type="button"
              className="btn btn-link text-decoration-none"
            >
              Kembali
            </button>
            <button
              // disabled={!title || !program.name || !image ? true : false}
              type="button"
              className="btn btn-primary ms-3"
              onClick={handleShow}
            >
              <i className="bi bi-eye-fill me-3"></i>Pratinjau
            </button>
          </div>
        </div>
      </section>
      <Modal show={show} fullscreen={fullscreen} onHide={() => setShow(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Pratinjau</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <section>
            <img
              src={imagePreview}
              className="img-fluid"
              style={{
                height: 500,
                objectFit: "cover",
                width: "100%",
                filter: "brightness(30%)",
                top: 0,
              }}
              alt="image artikel"
            />
            <div className="container py-5 h-100">
              <div style={{ position: "absolute", top: 350, color: "white" }}>
                <span className="badge bg-info mb-1">{program.name}</span>
                <h3>{title}</h3>
                <p>Waktu dan tanggal</p>
              </div>

              {/* {parse(article.content)} */}
              {show === true && parse(previewContent)}
              <br />
              <br />
              <br />
              <div className="d-flex justify-content-end mt-5">
                <button
                  type="button"
                  className="btn btn-link text-decoration-none"
                  onClick={() => setShow(false)}
                >
                  Kembali
                </button>
                <button
                  disabled={loadingSubmit === true ? true : false}
                  type="button"
                  className="btn btn-primary ms-3 col-2"
                  onClick={submitEditArticle}
                >
                  {loadingSubmit === true ? "Loading..." : "Publikasi"}
                </button>
              </div>
            </div>
          </section>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default EditArticle;
