import React from "react";
import Router from "next/router";
import axios from "axios";
import { STAGING_API } from "../api/axiosUrl";

const AuthContext = React.createContext();
const { Provider } = AuthContext;

const AuthProvider = ({ children }) => {
  const [authState, setAuthState] = React.useState({
    token: "",
  });
  const [contextLoading, setContextLoading] = React.useState(true);
  const [isAuth, setIsAuth] = React.useState(false);
  const [userInfo, setUserInfo] = React.useState({
    name: "",
    email: "",
    role: "",
  });
  const [status, setStatus] = React.useState("Semua Order");
  const [page, setPage] = React.useState(1);

  React.useEffect(() => {
    // Perform localStorage action
    const token = localStorage.getItem("accessToken");
    const name = localStorage.getItem("name");
    const email = localStorage.getItem("email");
    const role = localStorage.getItem("role");
    setAuthState({ token: token });
    setUserInfo({
      name,
      email,
      role,
    });
    isUserAuthenticated();
  }, []);

  const setUserAuthInfo = (res) => {
    localStorage.setItem("accessToken", res.data.data.token.accessToken);
    localStorage.setItem("name", res.data.data.name);
    localStorage.setItem("email", res.data.data.email);
    localStorage.setItem("role", res.data.data.role.name);
    setUserInfo({
      name: res.data.data.name,
      email: res.data.data.email,
      role: res.data.data.role.name,
    });

    setAuthState({
      token: res.data.data.token.accessToken,
    });
  };

  // checks if the user is authenticated or not
  const isUserAuthenticated = async () => {
    // !!authState.token;
    try {
      const accessToken = localStorage.getItem("accessToken");
      if (!accessToken) {
        setIsAuth(false);
        Router.push("/");
      }
      const response = await axios.get(
        `${STAGING_API}/api/v1/auth/secure`,

        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }
      );
      response.data.success === true
        ? setIsAuth(true)
        : setIsAuth(false) && Router.push("/");
      setContextLoading(false);
    } catch (error) {
      setContextLoading(false);
      console.log(error.response);
    }
  };

  const logout = () => {
    localStorage.removeItem("accessToken");
    localStorage.removeItem("name");
    localStorage.removeItem("email");
    localStorage.removeItem("role");
    setAuthState({ token: "" });
    setUserInfo({
      name: "",
      email: "",
      role: "",
    });
    setIsAuth(false);
  };

  return (
    <Provider
      value={{
        authState,
        isAuth,
        userInfo,
        contextLoading,
        status,
        page,
        setPage,
        setStatus,
        setIsAuth,
        setAuthState: (userAuthInfo) => setUserAuthInfo(userAuthInfo),
        isUserAuthenticated,
        logout,
      }}
    >
      {children}
    </Provider>
  );
};

export { AuthContext, AuthProvider };
