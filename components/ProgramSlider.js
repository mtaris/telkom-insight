import React, { useState, useEffect } from "react";
import Slider from "react-slick";
import axios from "axios";
import Link from "next/link";
import { STAGING_API } from "../api/axiosUrl";
import parse from "html-react-parser";

export default function ProgramSlider({ loading }) {
  const [programs, setPrograms] = useState([]);

  const handleFetchPrograms = async () => {
    try {
      const urlRes = await axios.get(`${STAGING_API}/api/v1/programs`);
      setPrograms(urlRes.data.data);
      console.log(urlRes.data.data);
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    handleFetchPrograms();
  }, []);

  const settings = {
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "0",
    slidesToShow: 3,
    speed: 500,
    // focusOnSelect: true,
    // variableWidth: true,
  };
  return (
    <div className="container">
      <Slider {...settings}>
        {loading ? (
          <div>
            <h4 className="text-center">Loading...</h4>
          </div>
        ) : (
          programs.map((e, index) => (
            <div className="col p-5" key={index}>
              <Link href={`programs/${e._id}`}>
                <div
                  className="card shadow mb-5 bg-body rounded"
                  style={{ cursor: "pointer" }}
                >
                  <img
                    style={{ height: 250, objectFit: "cover" }}
                    src={e.image.link}
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body text-center">
                    <h5 className="card-title ">{e.name}</h5>
                    {e.content.length > 100
                      ? parse(e.content.slice(0, 100) + "...")
                      : parse(e.content)}
                  </div>
                </div>
              </Link>
            </div>
          ))
        )}
      </Slider>
    </div>
  );
}
