import Image from "next/image";
import Link from "next/link";
import Router from "next/router";
import { Dropdown, Navbar, Container, Nav, NavDropdown } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";
import { AuthContext } from "../components/authContext";

function AdminNavBar() {
  const authContext = useContext(AuthContext);
  return (
    <>
      <nav className="navbar navbar-expand-lg px-5" style={{ height: 70 }}>
        <div className="container-fluid">
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
            <Link href={"/"}>
              <a className="navbar-brand me-auto">
                <Image
                  src="/images/logo.png"
                  alt="Logo Telkom Insight"
                  width={135}
                  height={64}
                />
              </a>
            </Link>

            <ul className="navbar-nav mb-2 mb-lg-0">
              {authContext.contextLoading === false && authContext.isAuth ? (
                <>
                  <li className="nav-item me-3">
                    <Link href={"/"}>
                      <a
                        className="nav-link text-dark list-group-item-action"
                        aria-current="page"
                      >
                        Home
                      </a>
                    </Link>
                  </li>
                  <li className="nav-item me-3">
                    <Link href={"/articles"}>
                      <a className="nav-link text-dark list-group-item-action">
                        Artikel
                      </a>
                    </Link>
                  </li>
                  <li className="nav-item me-3">
                    <Link href={"/programs"}>
                      <a className="nav-link text-dark list-group-item-action">
                        Program
                      </a>
                    </Link>
                  </li>
                  <li className="nav-item me-3">
                    <Link href={"/orders"}>
                      <a className="nav-link text-dark list-group-item-action">
                        Kelola Visit
                      </a>
                    </Link>
                  </li>
                  <li className="nav-item me-3">
                    <Link href={"/calendar"}>
                      <a className="nav-link text-dark list-group-item-action">
                        Kalender Kunjungan
                      </a>
                    </Link>
                  </li>
                  <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic">
                      {authContext.userInfo.name}
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item disabled>
                        <h5>{authContext.userInfo.role}</h5>
                        <small>{authContext.userInfo.email}</small>
                      </Dropdown.Item>
                      <Dropdown.Divider />
                      <Dropdown.Item>Edit Profil</Dropdown.Item>

                      <Dropdown.Item
                        className="text-danger"
                        onClick={() => {
                          authContext.logout();
                          Router.push("/");
                        }}
                      >
                        Logout
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </>
              ) : (
                <>
                  {/* <li className="nav-item me-3">
                    <Link href={"/register"}>
                      <button className="btn btn-link text-decoration-none btn-md">
                        Register
                      </button>
                    </Link>
                  </li> */}
                  <li className="nav-item me-3">
                    <Link href={"/loginadmin"}>
                      <button
                        className="btn btn-outline-light"
                        style={{ background: "#1D3072" }}
                      >
                        Login
                      </button>
                    </Link>
                  </li>
                </>
              )}
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}
export default AdminNavBar;
